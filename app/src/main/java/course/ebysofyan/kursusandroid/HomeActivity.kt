package course.ebysofyan.kursusandroid

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val sharedPreferences = getSharedPreferences("SESSION", Context.MODE_PRIVATE)
        val username = sharedPreferences.getString("USERNAME", "")

        text_welcome.text = "Selamat datang, ${username}"

        val user = intent.getParcelableExtra<Data>("USER")
        Toast.makeText(this, user?.username, Toast.LENGTH_LONG).show()

        button_logout.setOnClickListener {
            sharedPreferences.edit().clear().apply()

            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

}