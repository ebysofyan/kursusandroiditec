package course.ebysofyan.kursusandroid

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_data.*
import kotlinx.android.synthetic.main.data_item_view.view.*
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase



class DataActivity : AppCompatActivity() {

    private val data = mutableListOf<Person>()
    private lateinit var myadapter: DataAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data)

        rc_view.layoutManager = LinearLayoutManager(this)
        myadapter = DataAdapter(data)
        rc_view.adapter = myadapter

        button_simpan.setOnClickListener {
            val person = Person()

            if(input_name.text.toString().length > 5) {
                person.nama = input_name.text.toString()
            } else {
                input_name.error = "Nama tidak boleh kurang dari 5 karakter"
                return@setOnClickListener
            }


            person.gender = input_gender.text.toString()
            person.golonganDarah = input_golongan.text.toString()

            myadapter.tambahData(person)
            simpanFirebase(person)

            input_name.text.clear()
            input_gender.text.clear()
            input_golongan.text.clear()
            Toast.makeText(this, "Data berhasil di tambah!", Toast.LENGTH_LONG).show()
        }
    }

    // Penyimpanan ke Firebase
    private fun simpanFirebase(person: Person) {
        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("data")

        val key = myRef.push().key

        myRef.child("${key}").setValue(person)
    }
}

class DataAdapter(val data: MutableList<Person>) :
    RecyclerView.Adapter<DataAdapter.DataViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): DataViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.data_item_view, p0, false)
        return DataViewHolder(view, this)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(p0: DataViewHolder, p1: Int) {
        p0.isiData(data[p1])
    }

    fun tambahData(person: Person) {
        data.add(person)
        notifyDataSetChanged()
    }

    fun hapusData(index: Int) {
        data.removeAt(index)
        notifyDataSetChanged()
    }

    class DataViewHolder(view: View, private val adapter: DataAdapter) : RecyclerView.ViewHolder(view) {

        fun isiData(person: Person) {
            itemView.text_nama.text = person.nama
            itemView.text_gender.text = person.gender
            itemView.text_golongan.text = person.golonganDarah

            itemView.button_hapus.setOnClickListener {
                AlertDialog.Builder(it.context)
                    .setTitle("Yakin mau hapus?")
                    .setMessage("Hapus data ${person.nama}")
                    .setPositiveButton("Ok, hapus") { dialog, _ ->
                        adapter.hapusData(adapterPosition)
                        Toast.makeText(itemView.context, "Data berhasil di hapus!", Toast.LENGTH_LONG).show()
                        dialog.dismiss()
                    }
                    .setNegativeButton("Batal") { dialog, _ ->
                        dialog.dismiss()
                    }
                    .show()
            }

            itemView.setOnClickListener {
                Toast.makeText(itemView.context, person.nama, Toast.LENGTH_LONG).show()

                val intent = Intent(itemView.context, DetailActivity::class.java)
                val bundle = Bundle()
                bundle.putParcelable("PERSON", person)
                intent.putExtras(bundle)

                itemView.context.startActivity(intent)
            }
        }
    }
}