package course.ebysofyan.kursusandroid

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Data(
    val username: String? = "",
    val password: String? = "",
    val isLoggedIn: Boolean = false
) : Parcelable

@Parcelize
data class Person(
    var nama: String = "",
    var gender: String = "",
    var golonganDarah: String? = null
) : Parcelable
6
