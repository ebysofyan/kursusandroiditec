package course.ebysofyan.kursusandroid

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private val USERNAME = "hello"
    private val PASSWORD = "world"

    private lateinit var message: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val sudahLogin = getSharedPreferences("SESSION", Context.MODE_PRIVATE).getBoolean("IS_LOGGED_IN", false)
        if (sudahLogin) {
            startActivity(Intent(this, HomeActivity::class.java))
        }

        button_login.setOnClickListener {
            val username = txt_username.text.toString()
            val password = txt_password.text.toString()

            if (username == USERNAME && password == PASSWORD) {
                message = "Login berhasil!"

                val sharedPreferences = getSharedPreferences("SESSION", Context.MODE_PRIVATE).edit()
                sharedPreferences.putString("USERNAME", username)
                sharedPreferences.putBoolean("IS_LOGGED_IN", true)
                sharedPreferences.apply()

                val intent = Intent(this, HomeActivity::class.java)
                val user = Data(username, password, true)

                val bundle = Bundle()
                bundle.putParcelable("USER", user)
                intent.putExtras(bundle)

                startActivity(intent)
            } else {
                message = "Login gagal!"
            }

            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        }
    }
}
